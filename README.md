# TO-DO LIST LARAVEL

Application To-Do List avec le framework PHP : LARAVEL

---

<br>

Cas d'utilisation :  

```plantuml
@startuml
left to right direction


actor "utilisateur" as user

rectangle To-Do {
  usecase "Voir les tâches" as C1
  usecase "Ajouter tâche" as C2
  usecase "Modifier tâche" as C3
  usecase "Supprimer tâche" as C4
  usecase "Changer l'ordre des tâches" as C5
  usecase "Valider la tâche" as C6
}

user --> C1 
user --> C2
user --> C3 
user --> C4
user --> C5
user --> C6

@enduml

```
<br><br>

Page d'accueil :  
![accueil](images/index.PNG)

Nous pouvons ajouter une tâche :  
![ajout](images/ajout.PNG)
![ajout2](images/ajout2.PNG)

Nous pouvons modifier une tâche :  
![modif](images/modif.PNG)
![modif2](images/modif2.PNG)

Nous pouvons valider une tâche pour indiquer qu'elle a été accomplie :  
![valider](images/valider.PNG)
Puis l'invalider :  
![invalider](images/invalider.PNG)

Enfin, nous pouvons la réordonner :  
![reordonner](images/reorder.PNG)
![reordonner2](images/reorder2.PNG)