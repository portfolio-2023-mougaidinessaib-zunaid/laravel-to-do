<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TaskController::class, "index"])->name("index");
Route::get('/supprimer/{id}',[TaskController::class,"supprimer"])->whereNumber("id")->name("supprimer");

Route::get('/valider/{id}',[TaskController::class,"valider"])->whereNumber("id")->name("valider");
Route::get('/invalider/{id}',[TaskController::class,"invalider"])->whereNumber("id")->name("invalider");

Route::get('/formulaire',[TaskController::class, "formulaire"])->name("formulaire");
Route::post('/formulaire',[TaskController::class,"ajout"])->name("ajout");

Route::get('/edit/{id}',[TaskController::class,"edit"])->whereNumber("id")->name("modification");
Route::post('/edit/{id}',[TaskController::class,"save_edit"])->whereNumber("id")->name("save.edit");

Route::get('/reorder',[TaskController::class, "reorder"])->name("reorder");
Route::post('/reorder',[TaskController::class,"reorderSave"])->name("reorder.save");