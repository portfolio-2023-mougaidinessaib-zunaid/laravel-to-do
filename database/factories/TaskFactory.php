<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "text" => $this->faker->sentence,
            "done" =>$this->faker->boolean,
            "order" =>$this->faker->numberBetween($min = 1, $max = 100)
        ];
    }
}
