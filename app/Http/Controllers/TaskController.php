<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\LoggingModel;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(){
        $tasks=Task::orderBy('order', 'asc')->get();
        return view('task-index',[
            "tasks" => $tasks
        ]);
    }

    public function formulaire(){
        $titre="Ajout d'une tâche";
        return view('task-form',compact('titre'));
    }

    public function reorder(){
        $tasks=Task::where('done', 'false')->orderBy('order', 'asc')->get();
        
        return view('task-reorder',[
            'tasks'=>$tasks
        ]);
    }

    public function reorderSave(Request $request){
        $request->validate([
            "ids"=>"required",
            "orders"=>"required",
        ]);
        $index=0;
        $ids=$request->ids;
        $orders=$request->orders;
        //dd($orders);
        foreach($ids as $id){
            // $task = Task::find($id);
            // $task->order=$orders[$index];
            // $task->save();
            // $index+=1;

            $form_data = [
                'order' => $orders[$index],
                'id'    => $ids[$index],
            ];
            $task = Task::find($id);
            //dd($id);
            $ordre=$form_data['order'];
            //dd($ordre);
            $task->order=$ordre;
            $task->save();
            $index++;
        }
        $log=LoggingModel::create([
            'action'=>'réordonner',
            'identite'=>''
        ]);
        $log->save();
        return redirect('/')->with('reorder','Tâches réordonnées !');
    }

    public function supprimer($id){
        Task::destroy($id);
        $log=LoggingModel::create([
            'action'=>'supprime',
            'identite'=>''
        ]);
        $log->save();
        return redirect('/')->with('delete','Tâche supprimée !');
    }

    public function valider($id){
        $task=Task::find($id);
        $task->done=true;
        $task->save();
        return redirect('/');
    }

    public function invalider($id){
        $task=Task::find($id);
        $task->done=false;
        $task->save();
        return redirect('/');
    }

    public function ajout(Request $request){
        $request->validate([
            "text"=>"required",
            "order"=>"required",
        ]);
        $task=Task::create([
            'text' => $request->text,
            'order'=>$request->order,
            'done'=>false
        ]);
        $log=LoggingModel::create([
            'action'=>'ajout',
            'identite'=>''
        ]);
        $log->save();
        $task->save();
        return redirect('/')->with('ajout','Tâche ajoutée !');
    }

    public function edit($id){
        $titre="Modification d'une tâche";
        $task=Task::find($id);
        return view("task-form",[
            'titre'=>$titre,
            'task'=>$task
        ]);
    }

    public function save_edit(Request $request, $id){
        $request->validate([
            "text"=>"required",
            "order"=>"required",
        ]);
        $log=LoggingModel::create([
            'action'=>'modification',
            'identite'=>''
        ]);
        $log->save();
        $task=Task::find($id);
        $task->text=$request->text;
        $task->order=$request->order;

        $task->save();
        return redirect('/')->with('modif','Tâche modifiée !');
        
    }

}
