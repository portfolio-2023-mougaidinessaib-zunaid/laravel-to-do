<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoggingModel extends Model
{
    use HasFactory;

    protected $table = 'logging';
    protected $fillable = ['action','identite'];
}
