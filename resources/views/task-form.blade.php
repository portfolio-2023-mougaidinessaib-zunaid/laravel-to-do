@extends("base")
@section("content")
<div class="card">
    <div class="card-header">
        {{ $titre }}
    </div>
    <div class="card-body">
        @if ($titre=="Ajout d'une tâche")
            
        <form class ="form-horizontal" action="{{ route("ajout") }}" method="post">
            @csrf
            <div class=form-group>
                <form-label for="text">Text : </form-label>
                <input type="text" name="text" id="text" placeholder="Au boulot">
                <br>
                <form-label for="text">Ordre : </form-label>
                <input type="number" name="order" id="order" placeholder="Un numéro">
            </div>
            <button class="btn btn-primary" type="submit">
                <i class="fa fa-plus">Valider</i>
            </button>
        </form>
        @else
        <form class ="form-horizontal" action="{{ route("save.edit",['id'=>$task->id]) }}" method="post">
            @csrf
            <div class=form-group>
                <form-label for="text">Text : </form-label>
                <input type="text" name="text" id="text" value="{{ $task->text }}" placeholder="Au boulot">
                <br>
                <form-label for="text">Ordre : </form-label>
                <input type="number" name="order" id="order" max="999" min="1" value="{{ $task->order }}" placeholder="Un numéro">
            </div>
            <button class="btn btn-primary" type="submit">
                <i class="fa fa-plus">Valider</i>
            </button>
        </form>
        @endif
    </div>
</div>
@endsection
