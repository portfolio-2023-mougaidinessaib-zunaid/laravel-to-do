@extends('base')
@section('content')
    <div class="card">
        <div class="car-header">
            <div class="container">
                <h1 class="font-bold">Les tâches</h1>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-hover table-striped">
                @if (session('delete'))
                    <div class="alert alert-danger">
                        {{ session('delete') }}
                    </div>
                @endif
                @if (session('ajout'))
                    <div class="alert alert-success">
                        {{ session('ajout') }}
                    </div>
                @endif
                @if (session('modif'))
                    <div class="alert alert-info">
                        {{ session('modif') }}
                    </div>
                @endif
                @if (session('reorder'))
                    <div class="alert alert-success">
                        {{ session('reorder') }}
                    </div>
                @endif
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tâche</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>

                    @if ($tasks->count() > 0)
                        @foreach ($tasks as $task)
                            <tr>
                                <th scope="row">{{ $task->order }}</th>
                                @if ($task->done == true)
                                    <td><s>{{ $task->text }}<s></td>
                                @else
                                    <td>{{ $task->text }}</td>
                                @endif
                                <td>
                                    
                                    @if ($task->done == true)
                                        <a href="" class="btn btn-primary" role="button">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('invalider', ['id' => $task->id]) }}" class="btn btn-secondary"
                                            role="button">
                                            <i class="bi bi-x-square"></i>
                                        </a>
                                    @else
                                        <a href="{{ route('modification', ['id' => $task->id]) }}" class="btn btn-primary"
                                            role="button">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('valider', ['id' => $task->id]) }}" class="btn btn-secondary"
                                            role="button">
                                            <i class="fas fa-check-square"></i>
                                        </a>
                                    @endif
                                    <a href="{{ route('supprimer', ['id' => $task->id]) }}" class="btn btn-danger"
                                        role="button">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <th scope="row"></th>
                            <td>Aucune tâche</td>
                            <td><
                        </tr>
                    @endif

                </tbody>
            </table>
        </div>
        <div class="card-footer">
            <div class="col-12">
                <a class="btn btn-primary" href="{{ route('formulaire') }}"><i class="fas fa-plus">Ajouter</i></a>
                <a href='{{ route('reorder') }}' class="btn btn-secondary active" role="button">
                    <i class="fa fa-sort-numeric-asc" aria-hidden="true">Réordonner</i>
                </a>
            </div>
        </div>
    </div>
@endsection
