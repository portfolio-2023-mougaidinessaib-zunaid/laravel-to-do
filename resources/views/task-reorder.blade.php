@extends("base")
@section("content")
<div class="card">
    <form class="form-horizontal" action="{{ route('reorder.save') }}" method="post">
        @csrf
        <div class="card-header">
            Réordonner les tâches
        </div>
        <div class="card-body">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th scope="col">ordre</th>
                        <th scope="col">Tâche</th>
                    </tr>
                </thead>
                @foreach ($tasks as $task)
                    <tbody>
                        <tr>
                            <td>
                                <input type="hidden" class="form-control" name="ids[]" value="{{ $task->id }}" >
                                <input type="text" name="orders[]" value="{{ $task->order }}" >
                            </td>
                            <td>
                                <div class=form-control>
                                        {{ $task->text }}
                                </div>
                            </td>
                        </tr> 
                    </tbody>
                @endforeach
                
            </table>
        </div>
        <div class=card-footer>
            <div class=col-12>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                    <i class="fa fa-sort-numeric-asc" aria-hidden="true">Réordonner</i>
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
